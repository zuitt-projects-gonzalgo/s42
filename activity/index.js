const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
const click = document.querySelector("#click");

click.addEventListener('click', (event) => {
	spanFullName.innerHTML = (`${txtFirstName.value} ${txtLastName.value}`)
})